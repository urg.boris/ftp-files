using System.Collections;
using System.Collections.Generic;
using Sense3d.Fileloading.Monos;
using UnityEngine;

namespace Sense3d.Fileloading.Demo
{
    public class RemoteFilesDemo : RemoteFilesMono
    {
        public override void PrepareDownloadTasks()
        {
          
        }

        public override void PrepareLoadTasks()
        {
           
        }

        public override void AfterLoadTasks()
        {
           
        }

        public override void UpdateRemoteFileLinksFromLoadedConfig()
        {
          
        }

        public override void SetGameConfig(string json)
        {
           
        }
    }
}
