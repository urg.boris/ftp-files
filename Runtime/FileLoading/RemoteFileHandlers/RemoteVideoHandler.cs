﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Sense3d.Fileloading.RemoteHandlers;
using UnityEngine.Networking;
using System.Threading.Tasks;

namespace Sense3d.Fileloading
{
    [CreateAssetMenu(fileName = "RemoteVideo", menuName = "RemoteFileHandlers/Video")]
    public class RemoteVideoHandler : RemoteFileHandler
    {
        public string placeholder;

        [HideInInspector]
        public string content;


        public override async void DownloadRemoteFile()
        {
            await DownloadTask();

            LoadLocalContent();

        }

        public async Task<string> LoadLocallyTask()
        {
            return GetSavePathWithProtocol();
        }

        public async Task<bool> LoadLocalContentAsync()
        {
            if (ExistsFileLocally())
            {
                content = await LoadLocallyTask();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
            return true;
        }

        public string GetAvailableContent()
        {
            return content;
        }

        public override void LoadLocalContent()
        {
            if (ExistsFileLocally())
            {
                content = GetSavePathWithProtocol();
            }
            else
            {
                content = placeholder;
                FileLoadedWithPlaceholder(GetSavePath());
            }
        }

        public override Task<bool> LoadLocalContentBlocking()
        {
            throw new NotImplementedException();
        }


        public override async Task<(bool, string)> DownloadTask()
        {
            var urlPath = GetUrlPath();
            if (fileName == "")
            {
                return (true, "skipped:" + name);
            }
            FileDownloadStarted(urlPath);
            UnityWebRequest www = UnityWebRequest.Get(urlPath);
            bool resultOk = await HandleWWWRequest(www) != null;
            if (resultOk)
            {
                string savePath = GetSavePath();
                CreateDirsForFilePath(savePath);
                System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);

                //FileDownloaded(urlPath);

            }
            return (resultOk, urlPath);
        }


        public override void DisposeContent()
        {
            content = "";
        }



    }
}
