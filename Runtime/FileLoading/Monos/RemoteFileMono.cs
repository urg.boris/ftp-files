﻿using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Sense3d.Fileloading.

namespace Sense3d.Fileloading.Monos
{
    public class RemoteFileMono : ARemoteMono
    {
        //[HideInInspector]
        public string fileName;
        public RemoteFileHandler remoteFileHandler;

        public override void AddjustUrlPath()
        {
            urlPath =  GetPath();
            //remoteFileHandler.relativeUrl
           // Debug.Log(name+"=name, path=" + urlPath);
            remoteFileHandler.AddjustPath(urlPath);
        }

        public override string GetPathName()
        {
            return fileName;
        }

        public override void LoadStructure()
        {
            LoadBaseStructure();
            fileName = remoteFileHandler.fileName;
        }

    }
}
