﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Sense3d.Fileloading.DataConfigs
{
    [Serializable]
    class Slide
    {
        public string path;
    }
    class Gallery
    {
        public Slide[] slides;
    }
}


