using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sense3d.Fileloading.UI
{
    [AddComponentMenu("RemoteFileUI/RemoteImageUI")]
    public class RemoteImageUI : MonoBehaviour
    {
        public Image imageField;
        public RemoteImageHandler remoteFileHandler;
        protected void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }

}
