using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Sense3d.Fileloading.UI
{
    [AddComponentMenu("RemoteFileUI/RemoteVideoUI")]
    [RequireComponent(typeof(VideoPlayer))]
    [RequireComponent(typeof(RawImage))]
    public class RemoteVideoUI : MonoBehaviour
    {
        public VideoPlayer videoPlayer;
        public RawImage rawImage;
        public RemoteVideoHandler remoteFileHandler;

        private void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }


}
