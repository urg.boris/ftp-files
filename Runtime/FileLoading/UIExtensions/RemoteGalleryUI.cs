using Sense3d.Fileloading.DataConfigs;
using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sense3d.Fileloading.UI
{
    [AddComponentMenu("RemoteFileUI/RemoteGalleryUI")]
    public class RemoteGalleryUI : MonoBehaviour
    {
        public Image imageField;
        public RemoteGalleryHandler remoteFileHandler;

        protected void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }
}
