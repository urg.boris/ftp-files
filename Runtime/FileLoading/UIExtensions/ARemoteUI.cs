using Sense3d.Fileloading.RemoteHandlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sense3d.Fileloading.UI
{
    public abstract class ARemoteUI : MonoBehaviour
    {
        public RemoteFileHandler remoteFileHandler;



        private void Awake()
        {
#if !UNITY_EDITOR
            remoteFileHandler.LoadLocalContent();
#endif
        }
    }


}
