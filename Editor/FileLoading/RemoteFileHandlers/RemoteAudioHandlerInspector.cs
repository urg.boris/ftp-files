using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteAudioHandler))]
    public class RemoteAudioHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteAudioHandler remoteFileHandler = (RemoteAudioHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("Try DownloadFile"))
            {
                remoteFileHandler.DownloadRemoteFile();
            }
        }

    }
}
