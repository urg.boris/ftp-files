using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.RemoteHandlers

{
    [CustomEditor(typeof(RemoteVideoHandler))]
    public class RemoteVideoHandlerInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();//Draw inspector UI of ImageEditor

            RemoteVideoHandler remoteFileHandler = (RemoteVideoHandler)target;
            if (GUILayout.Button("Update current content"))
            {
                remoteFileHandler.LoadLocalContent();
            }
            if (GUILayout.Button("Try DownloadFile"))
            {
                remoteFileHandler.DownloadRemoteFile();
            }
        }

    }
}
