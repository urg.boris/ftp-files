﻿using UnityEditor;
using UnityEngine;


namespace Sense3d.Fileloading.Monos
{
    [CustomEditor(typeof(RemoteFileMono), true)]
    public class RemoteFileMonoInspector : Editor
    {
        SerializedProperty m_IntProp;

        public override void OnInspectorGUI()
        {
            RemoteFileMono tangerine = (RemoteFileMono) target;
            if (tangerine != null && tangerine.remoteFileHandler != null)
            {
                EditorGUILayout.TextField("relative url path", tangerine.remoteFileHandler.relativeUrl);
            }

            base.OnInspectorGUI(); //Draw inspector UI of ImageEditor
        }
    }
}