using Sense3d.Fileloading.RemoteHandlers;
using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.UI

{
    [CustomEditor(typeof(RemoteGalleryUI))]
    public class RemoteGalleryUIInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            RemoteGalleryUI targetComponent = (RemoteGalleryUI)target;
            /*  EditorGUILayout.BeginHorizontal();
              EditorGUILayout.PrefixLabel("Placeholder");
              targetComponent.remoteFileHandler = (RemoteGalleryHandler)EditorGUILayout.ObjectField(targetComponent.remoteFileHandler, typeof(RemoteGalleryHandler), false);
              EditorGUILayout.EndHorizontal();*/
            DrawDefaultInspector();
            if (targetComponent.remoteFileHandler != null)
            {
                if (GUILayout.Button("Show available content"))
                {
                    targetComponent.remoteFileHandler.LoadLocalContent();
                    Texture2D texture = targetComponent.remoteFileHandler.GetAvailableContent()[0];
                    targetComponent.imageField.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                }

                if (GUILayout.Button("Try DownloadFile and Update"))
                {
                    targetComponent.remoteFileHandler.DownloadRemoteFileFromInspector();
                }

               // base.OnInspectorGUI();
            }
        }
    }
}
