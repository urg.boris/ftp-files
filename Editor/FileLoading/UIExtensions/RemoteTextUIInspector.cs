using UnityEditor;
using UnityEngine;

namespace Sense3d.Fileloading.UI

{
    [CustomEditor(typeof(RemoteTextUI))]
    public class RemoteTextUIInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            //Draw inspector UI of ImageEditor

            RemoteTextUI targetComponent = (RemoteTextUI)target;
            /*
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("RemoteTextHandler");
            targetComponent.remoteFileHandler = (RemoteTextHandler)EditorGUILayout.ObjectField(targetComponent.remoteFileHandler, typeof(RemoteTextHandler), false);
            EditorGUILayout.EndHorizontal();
            */
            DrawDefaultInspector();
            if (targetComponent.remoteFileHandler != null)
            {
                if (GUILayout.Button("Show available content"))
                {
                    targetComponent.remoteFileHandler.LoadLocalContent();
                    targetComponent.text = targetComponent.remoteFileHandler.GetAvailableContent();
                }

                if (GUILayout.Button("Try DownloadFile and Update"))
                {
                    targetComponent.remoteFileHandler.DownloadRemoteFile();
                }
            }

            //base.OnInspectorGUI();

        }

    }
}
